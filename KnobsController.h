//
// Created by Dan on 13.05.2017.
//


#include "Const.h"
#include "JuliaSet.h"

typedef struct{
    unsigned char x;
    unsigned char y;
    unsigned char c;
    unsigned char alpha;
} KData;

void *map_phys_address_knobs(off_t region_base, size_t region_size, int opt_cached);

int readValues();

void initialize();

void processColor(KData *state, unsigned char* save, unsigned char current);

void process(JuliaInfo *info, char* textBuffer, int *showcase);

void exitKnobs();
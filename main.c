#include "Const.h"
#include "UDPController.c"
#include "KnobsController.c"
#include "LCDController.c"
#include "JuliaSet.h"


JuliaInfo *info;
int showcase = 0;
pthread_mutex_t lock;
char textBuffer[60];

void *knobs_thread(void *arg)
{
    initialize();
    while(1)
    {
        struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 10 * 1000 * 1000};
        pthread_mutex_lock(&lock);
        process(info, textBuffer, &showcase);
        pthread_mutex_unlock(&lock);
        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    }
    exitKnobs();
    return NULL;
}

/*
        struct timeval  tv;
        gettimeofday(&tv, NULL);
        double time_start = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000;
        gettimeofday(&tv, NULL);
        double time_end = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000;
        printf("LCD Time: %f || CenterX: %d || \"CenterY: %d\n", (time_end - time_start), info->centerX, info->centerY);

 */
void *lcd_thread(void *arg)
{
    pthread_mutex_lock(&lock);
    init(info);
    pthread_mutex_unlock(&lock);
    while (1) {
        // creating copy
        pthread_mutex_lock(&lock);
        JuliaInfo* bufferInfo = ((JuliaInfo*)malloc(sizeof(JuliaInfo)));\
        bufferInfo->centerX = info->centerX;
        bufferInfo->centerY = info->centerY;
        bufferInfo->size_x = info->size_x;
        bufferInfo->size_y = info->size_y;
        bufferInfo->maxIter = info->maxIter;
        bufferInfo->c[0] = info->c[0];
        bufferInfo->c[1] = info->c[1];
        bufferInfo->scale = info->scale;
        pthread_mutex_unlock(&lock);

        // processing with copied info
        processLCD(bufferInfo, textBuffer);
        free(bufferInfo);
    }
    exitLCD();
    return NULL;
}

void *udp_client_thread(void *arg) {
    struct sockaddr_in si_me, si_other;
    int s, slen = sizeof(si_other), recv_len;
    char buf[BUFLEN];
    float c_r, c_i;
    int x, y, maxIter;

    //create a UDP socket
    if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        die("socket");
    }
    // zero out the structure
    memset((char *) &si_me, 0, sizeof(si_me));

    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(PORT);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    //bind socket to port
    if (bind(s, (struct sockaddr *) &si_me, sizeof(si_me)) == -1) {
        die("bind");
    }

    //keep listening for data
    while (1) {
        fflush(stdout);

        //try to receive some data, this is a blocking call
        if ((recv_len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) == -1) {
            die("recvfrom()");
        }

        //print details of the client/peer and the data received
        printf("Received data from %s:%d\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));

        char buffer[10];
        if(sscanf(buf, "%d %d %f %f %d", &x, &y, &c_r, &c_i, &maxIter) == 5){
            printf("X: %d Y:%d CR:%f CI:%f MaxIter: %d\n", x,y,c_r,c_i, maxIter);
            pthread_mutex_lock(&lock);
            processUDP(info, textBuffer, x, y, c_r, c_i, maxIter);
            showcase = 0;
            pthread_mutex_unlock(&lock);
        } else if(sscanf(buf, "%s", buffer) == 1){
            if(strcmp(buffer, "show") == 0){
                pthread_mutex_lock(&lock);
                showcase = 1;
                pthread_mutex_unlock(&lock);
            }
        }
        // TODO add showcase state change

        memset(buf, '\0', BUFLEN); //Add this line
    }

    close(s);

    return NULL;
}


int main(int argc, char** argv)
{
    int soubor= open("/dev/mem",O_RDWR | O_SYNC);
    if(soubor==-1) {
        printf("Error opening file\n");
        return 1;
    }

    if (pthread_mutex_init(&lock, NULL) != 0)
    {
        printf("\n mutex init failed\n");
        return 1;
    }

    info = (JuliaInfo*) malloc(sizeof(JuliaInfo));
    memset(textBuffer, '\0', 32);
    sprintf(textBuffer, "Welcome!");
    // LCD thread
    pthread_t lcd;	// this is our thread identifier
    pthread_create(&lcd, NULL, lcd_thread, NULL);

    // knobs thread
    pthread_t knobs;	// this is our thread identifier
    pthread_create(&knobs, NULL, knobs_thread, NULL);

    // UDP server thread
    pthread_t udp_server;	// this is our thread identifier
    pthread_create(&udp_server, NULL, udp_client_thread, NULL);

    pthread_join(udp_server, NULL);
    pthread_join(knobs, NULL);
    pthread_join(lcd, NULL);

    pthread_mutex_destroy(&lock);
    free(info);
    return 0;
}

//
// Created by Dan on 13.05.2017.
//

#include "LCDController.h"
#include "JuliaSet.c"
#include "JuliaSet.h"
#include "font_rom8x16.c"
#include "font_types.h"

#define sizeX 480
#define sizeY 320

char *memdev_lcd="/dev/mem";
unsigned char *parlcd_mem_base;

Pixel *textColor;
Pixel *bgColor;


void *map_phys_address_lcd(off_t region_base, size_t region_size, int opt_cached)
{
    unsigned long mem_window_size;
    unsigned long pagesize;
    unsigned char *mm;
    unsigned char *mem;
    int fd;

    fd = open(memdev_lcd, O_RDWR | (!opt_cached? O_SYNC: 0));
    if (fd < 0) {
        fprintf(stderr, "cannot open %s\n", memdev_lcd);
        return NULL;
    }

    pagesize=sysconf(_SC_PAGESIZE);

    mem_window_size = ((region_base & (pagesize-1)) + region_size + pagesize-1) & ~(pagesize-1);

    mm = mmap(NULL, mem_window_size, PROT_WRITE|PROT_READ,
              MAP_SHARED, fd, region_base & ~(pagesize-1));
    mem = mm + (region_base & (pagesize-1));

    if (mm == MAP_FAILED) {
        fprintf(stderr,"mmap error\n");
        return NULL;
    }

    return mem;
}

void parlcd_write_cmd(unsigned char *parlcd_mem_base, uint16_t cmd)
{
    *(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_CMD_o) = cmd;
}

void parlcd_write_data(unsigned char *parlcd_mem_base, uint16_t data)
{
    *(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_DATA_o) = data;
}

void parlcd_write_data2x(unsigned char *parlcd_mem_base, uint32_t data)
{
    *(volatile uint32_t*)(parlcd_mem_base + PARLCD_REG_DATA_o) = data;
}

void parlcd_delay(int msec)
{
    struct timespec wait_delay = {.tv_sec = msec / 1000,
            .tv_nsec = (msec % 1000) * 1000 * 1000};
    clock_nanosleep(CLOCK_MONOTONIC, 0, &wait_delay, NULL);
}

uint16_t RGB888ToRGB565( Pixel pixel )
{
    int red   = pixel.r;
    int green = pixel.g;
    int blue  = pixel.b;

    unsigned short  B = (unsigned short) ((blue >> 3) & 0x001F);
    unsigned short  G = (unsigned short) (((green >> 2) << 5) & 0x07E0); // not <
    unsigned short  R = (unsigned short) (((red >> 3) << 11) & 0xF800); // not <

    return (uint16_t) (R | G | B);
}

void parlcd_hx8357_init(unsigned char *parlcd_mem_base)
{
    // toggle RST low to reset
/*
    digitalWrite(_rst, HIGH);
    parlcd_delay(50);
    digitalWrite(_rst, LOW);
    parlcd_delay(10);
    digitalWrite(_rst, HIGH);
    parlcd_delay(10);
*/
    parlcd_write_cmd(parlcd_mem_base, 0x1);
    parlcd_delay(30);

#ifdef HX8357_B
    // Configure HX8357-B display
    parlcd_write_cmd(parlcd_mem_base, 0x11);
    parlcd_delay(20);
    parlcd_write_cmd(parlcd_mem_base, 0xD0);
    parlcd_write_data(parlcd_mem_base, 0x07);
    parlcd_write_data(parlcd_mem_base, 0x42);
    parlcd_write_data(parlcd_mem_base, 0x18);

    parlcd_write_cmd(parlcd_mem_base, 0xD1);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x07);
    parlcd_write_data(parlcd_mem_base, 0x10);

    parlcd_write_cmd(parlcd_mem_base, 0xD2);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0x02);

    parlcd_write_cmd(parlcd_mem_base, 0xC0);
    parlcd_write_data(parlcd_mem_base, 0x10);
    parlcd_write_data(parlcd_mem_base, 0x3B);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x02);
    parlcd_write_data(parlcd_mem_base, 0x11);

    parlcd_write_cmd(parlcd_mem_base, 0xC5);
    parlcd_write_data(parlcd_mem_base, 0x08);

    parlcd_write_cmd(parlcd_mem_base, 0xC8);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x32);
    parlcd_write_data(parlcd_mem_base, 0x36);
    parlcd_write_data(parlcd_mem_base, 0x45);
    parlcd_write_data(parlcd_mem_base, 0x06);
    parlcd_write_data(parlcd_mem_base, 0x16);
    parlcd_write_data(parlcd_mem_base, 0x37);
    parlcd_write_data(parlcd_mem_base, 0x75);
    parlcd_write_data(parlcd_mem_base, 0x77);
    parlcd_write_data(parlcd_mem_base, 0x54);
    parlcd_write_data(parlcd_mem_base, 0x0C);
    parlcd_write_data(parlcd_mem_base, 0x00);

    parlcd_write_cmd(parlcd_mem_base, 0x36);
    parlcd_write_data(parlcd_mem_base, 0x0a);

    parlcd_write_cmd(parlcd_mem_base, 0x3A);
    parlcd_write_data(parlcd_mem_base, 0x55);

    parlcd_write_cmd(parlcd_mem_base, 0x2A);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0x3F);

    parlcd_write_cmd(parlcd_mem_base, 0x2B);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0xDF);

    parlcd_delay(120);
    parlcd_write_cmd(parlcd_mem_base, 0x29);

    parlcd_delay(25);

#else
// HX8357-C display initialisation

    parlcd_write_cmd(parlcd_mem_base, 0xB9); // Enable extension command
    parlcd_write_data(parlcd_mem_base, 0xFF);
    parlcd_write_data(parlcd_mem_base, 0x83);
    parlcd_write_data(parlcd_mem_base, 0x57);
    parlcd_delay(50);

    parlcd_write_cmd(parlcd_mem_base, 0xB6); //Set VCOM voltage
    //parlcd_write_data(parlcd_mem_base, 0x2C);    //0x52 for HSD 3.0"
    parlcd_write_data(parlcd_mem_base, 0x52);    //0x52 for HSD 3.0"

    parlcd_write_cmd(parlcd_mem_base, 0x11); // Sleep off
    parlcd_delay(200);

    parlcd_write_cmd(parlcd_mem_base, 0x35); // Tearing effect on
    parlcd_write_data(parlcd_mem_base, 0x00);    // Added parameter

    parlcd_write_cmd(parlcd_mem_base, 0x3A); // Interface pixel format
    parlcd_write_data(parlcd_mem_base, 0x55);    // 16 bits per pixel

    //parlcd_write_cmd(parlcd_mem_base, 0xCC); // Set panel characteristic
    //parlcd_write_data(parlcd_mem_base, 0x09);    // S960>S1, G1>G480, R-G-B, normally black

    //parlcd_write_cmd(parlcd_mem_base, 0xB3); // RGB interface
    //parlcd_write_data(parlcd_mem_base, 0x43);
    //parlcd_write_data(parlcd_mem_base, 0x00);
    //parlcd_write_data(parlcd_mem_base, 0x06);
    //parlcd_write_data(parlcd_mem_base, 0x06);

    parlcd_write_cmd(parlcd_mem_base, 0xB1); // Power control
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x15);
    parlcd_write_data(parlcd_mem_base, 0x0D);
    parlcd_write_data(parlcd_mem_base, 0x0D);
    parlcd_write_data(parlcd_mem_base, 0x83);
    parlcd_write_data(parlcd_mem_base, 0x48);


    parlcd_write_cmd(parlcd_mem_base, 0xC0); // Does this do anything?
    parlcd_write_data(parlcd_mem_base, 0x24);
    parlcd_write_data(parlcd_mem_base, 0x24);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0x3C);
    parlcd_write_data(parlcd_mem_base, 0xC8);
    parlcd_write_data(parlcd_mem_base, 0x08);

    parlcd_write_cmd(parlcd_mem_base, 0xB4); // Display cycle
    parlcd_write_data(parlcd_mem_base, 0x02);
    parlcd_write_data(parlcd_mem_base, 0x40);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x2A);
    parlcd_write_data(parlcd_mem_base, 0x2A);
    parlcd_write_data(parlcd_mem_base, 0x0D);
    parlcd_write_data(parlcd_mem_base, 0x4F);

    parlcd_write_cmd(parlcd_mem_base, 0xE0); // Gamma curve
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x15);
    parlcd_write_data(parlcd_mem_base, 0x1D);
    parlcd_write_data(parlcd_mem_base, 0x2A);
    parlcd_write_data(parlcd_mem_base, 0x31);
    parlcd_write_data(parlcd_mem_base, 0x42);
    parlcd_write_data(parlcd_mem_base, 0x4C);
    parlcd_write_data(parlcd_mem_base, 0x53);
    parlcd_write_data(parlcd_mem_base, 0x45);
    parlcd_write_data(parlcd_mem_base, 0x40);
    parlcd_write_data(parlcd_mem_base, 0x3B);
    parlcd_write_data(parlcd_mem_base, 0x32);
    parlcd_write_data(parlcd_mem_base, 0x2E);
    parlcd_write_data(parlcd_mem_base, 0x28);

    parlcd_write_data(parlcd_mem_base, 0x24);
    parlcd_write_data(parlcd_mem_base, 0x03);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x15);
    parlcd_write_data(parlcd_mem_base, 0x1D);
    parlcd_write_data(parlcd_mem_base, 0x2A);
    parlcd_write_data(parlcd_mem_base, 0x31);
    parlcd_write_data(parlcd_mem_base, 0x42);
    parlcd_write_data(parlcd_mem_base, 0x4C);
    parlcd_write_data(parlcd_mem_base, 0x53);
    parlcd_write_data(parlcd_mem_base, 0x45);
    parlcd_write_data(parlcd_mem_base, 0x40);
    parlcd_write_data(parlcd_mem_base, 0x3B);
    parlcd_write_data(parlcd_mem_base, 0x32);

    parlcd_write_data(parlcd_mem_base, 0x2E);
    parlcd_write_data(parlcd_mem_base, 0x28);
    parlcd_write_data(parlcd_mem_base, 0x24);
    parlcd_write_data(parlcd_mem_base, 0x03);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x01);

    parlcd_write_cmd(parlcd_mem_base, 0x36); // MADCTL Memory access control
    //parlcd_write_data(parlcd_mem_base, 0x48);
    parlcd_write_data(parlcd_mem_base, 0xE8);
    parlcd_delay(20);

    parlcd_write_cmd(parlcd_mem_base, 0x21); //Display inversion on
    parlcd_delay(20);

    parlcd_write_cmd(parlcd_mem_base, 0x29); // Display on

    parlcd_delay(120);
#endif
}

void initJulia(JuliaInfo *info){
    int maxIter;
    double scale = 0.8;
    int centerX, centerY;
    double c[2] = {0.2,-0.1};
    c[0] = 0.285;
    c[1] = 0;
    maxIter = 30;
    //computing screen center as half of the screen
    centerX = sizeX/2;
    centerY = sizeY/2;

    info->c[0] = c[0];
    info->c[1] = c[1];
    info->centerX = centerX;
    info->centerY = centerY;
    info->maxIter = maxIter;
    info->size_x = sizeX;
    info->size_y = sizeY;
    info->scale = scale;
}

void clearDisplay(){
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (int i = 0; i < sizeY ; i++) {
        for (int j = 0; j < sizeX ; j++) {
            parlcd_write_data(parlcd_mem_base, 0);
        }
    }
}

void setColor(Pixel *p1, Pixel *p2){
    p1->r = p2->r;
    p1->g = p2->g;
    p1->b = p2->b;
}

void put_char_there(JuliaImage* data, char c, int row, int column, Pixel *color, Pixel *background) {
    for (int i = 0; i < 16; i++) {
        setColor(&data->data[i*sizeX + row * 16 + column * 8], font_rom8x16.bits[(int) c * 16+ i] >> 15 & 1 ? color : background);
        setColor(&data->data[i*sizeX + row * 16 + column * 8 + 1], font_rom8x16.bits[(int) c * 16 + i] >> 14 & 1 ? color : background);
        setColor(&data->data[i*sizeX + row * 16 + column * 8 + 2], font_rom8x16.bits[(int) c * 16 + i] >> 13 & 1 ? color : background);
        setColor(&data->data[i*sizeX + row * 16 + column * 8 + 3], font_rom8x16.bits[(int) c * 16 + i] >> 12 & 1 ? color : background);
        setColor(&data->data[i*sizeX + row * 16 + column * 8 + 4], font_rom8x16.bits[(int) c * 16 + i] >> 11 & 1 ? color : background);
        setColor(&data->data[i*sizeX + row * 16 + column * 8 + 5], font_rom8x16.bits[(int) c * 16 + i] >> 10 & 1 ? color : background);
        setColor(&data->data[i*sizeX + row * 16 + column * 8 + 6], font_rom8x16.bits[(int) c * 16 + i] >> 9 & 1 ? color : background);
        setColor(&data->data[i*sizeX + row * 16 + column * 8 + 7], font_rom8x16.bits[(int) c * 16 + i] >> 8 & 1 ? color : background);
    }
}

void drawImage(JuliaImage *image, char* text){
    int len = strlen(text);
    for (int k = 0; k < len; ++k) {
        put_char_there(image, text[k], 1, k, textColor, bgColor);
    }
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (int i = 0; i < sizeY ; i++) {
        for (int j = 0; j < sizeX ; j++) {
            int actual = j+sizeX*i;
            parlcd_write_data(parlcd_mem_base, RGB888ToRGB565(image->data[actual]));
        }
    }
}

void processLCD(JuliaInfo *info, char *text){
    // creating new julia image
    JuliaImage* image = generateJuliaSet(info);
    drawImage(image, text);
    free(image);
}

void exitLCD(){
    free(textColor);
    free(bgColor);
}

void initColors(){
    textColor = ((Pixel*) malloc(sizeof(Pixel)));
    bgColor = ((Pixel*) malloc(sizeof(Pixel)));
    textColor->r=255;
    textColor->g=255;
    textColor->b=255;

    bgColor->r=30;
    bgColor->g=30;
    bgColor->b=30;

}

void init(JuliaInfo *info){
    parlcd_mem_base = map_phys_address_lcd(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (parlcd_mem_base == NULL)
        exit(1);

    parlcd_hx8357_init(parlcd_mem_base);
    clearDisplay();
    initJulia(info);
    initColors();
}

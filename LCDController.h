//
// Created by Dan on 13.05.2017.
//


#include "Const.h"
#include "JuliaSet.h"

void parlcd_write_cmd(unsigned char *parlcd_mem_base, uint16_t cmd);

void parlcd_write_data(unsigned char *parlcd_mem_base, uint16_t data);

void parlcd_write_data2x(unsigned char *parlcd_mem_base, uint32_t data);

void parlcd_delay(int msec);

void *map_phys_address_lcd(off_t region_base, size_t region_size, int opt_cached);

uint16_t RGB888ToRGB565(Pixel pixel);

void init(JuliaInfo *info);

void processLCD(JuliaInfo *info, char* text);

void exitLCD();

void drawImage(JuliaImage *image, char* text);

void clearDisplay();

void parlcd_hx8357_init(unsigned char *parlcd_mem_base);

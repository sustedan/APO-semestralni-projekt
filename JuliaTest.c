#include <stdio.h>
#include <stdlib.h>
#include "JuliaSet.h"


int sizeX = 3840, sizeY = 2160;
int size;
int maxIter;
double scale = 1.0;
int centerX, centerY;
double c[2] = {0,0};

void writePPMFile(const char *filename, Pixel *out) {
    FILE *file;
    file = fopen(filename, "wb");
    if (!file) {
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }
    fprintf(file, "P6\n");
    fprintf(file, "%d\n%d\n%d\n",sizeX, sizeY, 255);

    fwrite(out, 1, size*3, file);
    fclose(file);
}

void translateBy(int x, int y){
    centerX += x;
    centerY += y;
}


int main(int argc, char** argv) {
    if(argc < 4) return 0;
    // real part
    c[0] = atof(argv[1]);
    // imaginary part
    c[1] = atof(argv[2]);
    // max iterations
    maxIter = atoi(argv[3]);
    // image size
    size = sizeX*sizeY;
    //computing screen center as half of the screen
    centerX = sizeX/2;
    centerY = sizeY/2;

   // translateBy(100,200);
    JuliaInfo *info = (JuliaInfo*) malloc(sizeof(JuliaInfo));
    info->c[0] = c[0];
    info->c[1] = c[1];
    info->centerX = centerX;
    info->centerY = centerY;
    info->maxIter = maxIter;
    info->sizeX = sizeX;
    info->sizeY = sizeY;
    info->scale = scale;

    JuliaImage* image = generateJuliaSet(info[0]);
    writePPMFile("out.ppm", image->data);

    free(info);
    free(image);
    return 0;
}
//
// Created by Dan on 13.05.2017.
//

#include "KnobsController.h"
#include "JuliaSet.h"

char *memdev="/dev/mem";

unsigned char *knobs_mem_base;
KData *state;
double lastTime;

int presetsCount = 7;
double presets[7][3] = {
        {-0.7269, 0.1889, 600},
        {0, -0.8, 50},
        {0.285, 0, 55},
        {0.285, 0.01, 150},
        {-0.70176, -0.3842, 100},
        {-0.835, -0.2321, 80},
        {-0.8, 0.156, 250},
};


void *map_phys_address_knobs(off_t region_base, size_t region_size, int opt_cached)
{
    unsigned long mem_window_size;
    unsigned long pagesize;
    unsigned char *mm;
    unsigned char *mem;
    int fd;
    fd = open(memdev, O_RDWR | (!opt_cached? O_SYNC: 0));
    if (fd < 0) {
        fprintf(stderr, "cannot open %s\n", memdev);
        return NULL;
    }
    pagesize=sysconf(_SC_PAGESIZE);
    mem_window_size = ((region_base & (pagesize-1)) + region_size + pagesize-1) & ~(pagesize-1);
    mm = mmap(NULL, mem_window_size, PROT_WRITE|PROT_READ,
              MAP_SHARED, fd, region_base & ~(pagesize-1));
    if (mm == MAP_FAILED) {
        fprintf(stderr,"mmap error\n");
        return NULL;
    }
    mem = mm + (region_base & (pagesize-1));

    return mem;
}

int readValues(){
    int data = *(volatile uint32_t*)(knobs_mem_base + SPILED_REG_KNOBS_8BIT_o);
    return data;
}


void initialize(){
    knobs_mem_base = map_phys_address_knobs(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (knobs_mem_base == NULL)
        exit(1);

    state = ((KData*)malloc(sizeof(KData)));

    state->x = -128;
    state->y = -128;
    state->c = 0;
    state->alpha = 0;

    srand(time(NULL));
    struct timeval  tv;
    gettimeofday(&tv, NULL);
    double time_start = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000;
    lastTime = time_start;

}

void processColor(KData *state, unsigned char* save, unsigned char current){
    unsigned char past = ((*save) & MAX_INPUT_VAL);

    signed char diff = current - past;
    signed char signDiff = (diff < 0 ? -1 : (diff == 0 ? 0 : 1));
    unsigned char absDiff = (signDiff * diff);

    if(absDiff > MAX_INPUT_VAL - 2*THRESHOLD){
        absDiff = (MAX_INPUT_VAL - absDiff + 1);
        signDiff = -signDiff;
    }

    if(signDiff == 1){
        (*save) += absDiff;
    }else if(signDiff == -1){
        (*save) -= absDiff;
    }
}

void exitKnobs(){
    free(state);
}



void process(JuliaInfo *info, char* textBuffer, int *showcase){
    int color = readValues();
    unsigned char red    = (unsigned char) (((color & RED_MASK  ) >> 16 ) >> 2);
    unsigned char green  = (unsigned char) (((color & GREEN_MASK) >> 8  ) >> 2);
    unsigned char blue   = (unsigned char) ((color & BLUE_MASK ) >> 2);

	unsigned char oldX = state->x;
	unsigned char oldY = state->y;
	unsigned char oldC = state->c;
    processColor(state, &state->c, blue);
    processColor(state, &state->y, green);
    processColor(state, &state->x, red);

    struct timeval  tv;
    gettimeofday(&tv, NULL);
    double time = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000;

    if(time - lastTime > 2000 && *showcase == 1){
        int r = rand() % (presetsCount);
        info->c[0] = presets[r][0];
        info->c[1] = presets[r][1];
        info->maxIter = (int) presets[r][2];
        lastTime = time;
        memset(textBuffer, '\0', 60);
        sprintf(textBuffer, "PRESET:: X:%d | Y:%d | CR:%.2f | CI:%.2f | Iter:%d", state->x-128, state->y-128, info->c[0], info->c[1], info->maxIter);
    }

    //printf("KnobsRED: %d || KnobsGREEN: %d || KnobsBLUE: %d\n", state.x, state.y, state.state);
	if(oldX != state->x || oldY != state->y || oldC != state->c){
        *showcase = 0;
        info->centerX = 5*info->scale*(state->x - 128) + info->size_x/2;
        info->centerY = 5*info->scale*(state->y - 128) + info->size_y/2;
        double* preset = presets[state->c % presetsCount];
        info->c[0] = preset[0];
        info->c[1] = preset[1];
        info->maxIter = (int) preset[2];
        memset(textBuffer, '\0', 32);
        sprintf(textBuffer, "KNOBS:: X:%d | Y:%d | CR:%.2f | CI:%.2f | Iter:%d", state->x-128, state->y-128, info->c[0], info->c[1], info->maxIter);
    }
}

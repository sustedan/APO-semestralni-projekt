//
// Created by Dan on 04.05.2017.
//

#ifndef JULIASET_JULIASET_H
#define JULIASET_JULIASET_H

#define MAX_RANGE 2

#include "Const.h"

/**
 * Holder for pixel colors
 */
typedef struct {
    unsigned char r,g,b;
} Pixel;

typedef struct {
    int sizeX, sizeY;
    Pixel *data;
} JuliaImage;

/**
 * Holder for Julia Set info
 */
typedef struct {
    double c[2];
    int centerX, centerY;
    double scale;
    int maxIter;
    int size_x, size_y;
} JuliaInfo;

/**
 * Computes color ratio from given Julia Set info
 * @param info holder for Julia Set information
 * @param x position of the pixel
 * @param y position of the pixel
 * @return parameter t in range from 0 to 1
 */
double computeJuliaParam(JuliaInfo *info, int x, int y);

/**
 *
 * @param info holder with Julia Set info
 * @return pixel matrix with Julia Set printed
 */
JuliaImage* generateJuliaSet(JuliaInfo *info);

#endif //JULIASET_JULIASET_H

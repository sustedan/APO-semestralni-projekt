NAMEFILE = main
CC = gcc -Wall -pedantic -std=gnu99

all: program

program: main.o JuliaSet.o Knobs.o LCD.o UDPController.o
	$(CC) -o main main.o -lpthread

main.o: main.c JuliaSet.h
	$(CC) -c main.c

JuliaSet.o: JuliaSet.c JuliaSet.h
	$(CC) -c JuliaSet.c

Knobs.o: KnobsController.c KnobsController.h
	$(CC) -c KnobsController.c

LCD.o: LCDController.c LCDController.h
	$(CC) -c LCDController.c

UDPController.o: UDPController.c UDPController.h
	$(CC) -c UDPController.c

clean:
	rm -f $(NAMEFILE) JuliaSet.o main.o KnobsController.o LCDController.o UDPController.o


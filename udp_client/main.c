#include<stdio.h> //printf
#include<string.h> //memset
#include<stdlib.h> //exit(0);
#include<arpa/inet.h>
#include<sys/socket.h>
#include <pthread.h>

// udp
#define SERVER "192.168.202.167"
#define BUFLEN 512  //Max length of buffer
#define PORT 9999   //The port on which to send data

void die(char *s)
{
    perror(s);
    exit(1);
}

void *udp_client_thread(void *arg) {
    struct sockaddr_in si_other;
    int s, slen=sizeof(si_other);
    char buf[BUFLEN];
    char message[BUFLEN];

    if ( (s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        die("socket");
    }

    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(PORT);

    if (inet_aton(SERVER , &si_other.sin_addr) == 0)
    {
        fprintf(stderr, "inet_aton() failed\n");
        exit(1);
    }

    while(1)
    {
        printf("Enter params: <x> <y> <c_real> <c_imag> <max_iter>\n");
        gets(message);

        //send the message
        if (sendto(s, message, strlen(message) , 0 , (struct sockaddr *) &si_other, slen)==-1)
        {
            die("sendto()");
        }

        //receive a reply and print it
        //clear the buffer by filling null, it might have previously received data
        memset(buf,'\0', BUFLEN);

        puts(buf);
    }

    close(s);

    return NULL;
}

int main(int argc, char** argv)
{
    // UDP server thread
    pthread_t udp;	// this is our thread identifier
    pthread_create(&udp, NULL, udp_client_thread, NULL);

    pthread_join(udp, NULL);

    return 0;
}

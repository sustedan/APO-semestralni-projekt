//
// Created by Dan on 18.05.2017.
//

#include "UDPController.h"
#include "Const.h"
#include "JuliaSet.h"

void die(char *s)
{
    perror(s);
    exit(1);
}

void processUDP(JuliaInfo *info, char* textBuffer, int x, int y, float c_r, float c_i, int maxIter){
    info->centerX = info->scale*x + info->size_x/2;
    info->centerY = info->scale*y + info->size_y/2;
    info->c[0] = c_r;
    info->c[1] = c_i;
    info->maxIter = maxIter;
    memset(textBuffer, '\0', 32);
    sprintf(textBuffer, "UDP:: X:%d | Y:%d | CR:%.2f | CI:%.2f | Iter:%d", x, y, info->c[0], info->c[1], info->maxIter);
}


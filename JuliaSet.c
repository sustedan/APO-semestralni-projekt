//
// Created by Dan on 04.05.2017.
//

#include "JuliaSet.h"


double computeJuliaParam(JuliaInfo *info, int x, int y){
    double newZ[2];
    double oldZ[2];
    newZ[0] = (info->centerX-x) / (info->scale*info->size_x/2);
    newZ[1] = (y - info->centerY) / (info->scale*info->size_y/2);
    for (int i = 0; i < info->maxIter; ++i) {
        oldZ[0] = newZ[0];
        oldZ[1] = newZ[1];

        newZ[0] = oldZ[0]*oldZ[0]-oldZ[1]*oldZ[1] + info->c[0];
        newZ[1] = 2 * oldZ[0] * oldZ[1] + info->c[1];

        double val = (newZ[0] * newZ[0] + newZ[1] * newZ[1]);
        if(val > 4.0){
            return (i*1.0)/(1.0*info->maxIter);
        }
    }
    return 0.0;
}


JuliaImage* generateJuliaSet(JuliaInfo *info){
    int size = info->size_x * info->size_y;
    Pixel *imageData = (Pixel*)malloc(size*sizeof(Pixel));
    JuliaImage *image = (JuliaImage*) malloc(sizeof(JuliaImage));
    image->data = imageData;
    image->sizeX = info->size_x;
    image->sizeY = info->size_y;
    for(int y = 0; y < info->size_y; y++){
        for(int x = 0; x < info->size_x; x++) {
            int actual = x + info->size_x * y;
            double t = computeJuliaParam(info, x, y);
            /*image->data[actual].r = (unsigned char) (125.0 * t * 2);
            image->data[actual].g = (unsigned char) (100.0 * t + 40.0);
            image->data[actual].b = (unsigned char) (150.0 * 1.2 * t + 75.0);*/
            if (t > 0.45) {
                image->data[actual].r = (unsigned char) (125.0 * t + 80.0);
                image->data[actual].g = (unsigned char) (100.0 * t + 150.0);
                image->data[actual].b = (unsigned char) (50.0 * 2 * t + 110.0);
            } else {
                image->data[actual].r = (unsigned char) (60.0 * t * 4 + 20.0);
                image->data[actual].g = (unsigned char) (125.0 * t * 2 + 20.0);
                image->data[actual].b = (unsigned char) (50.0 * t * 3 + 50.0);
            }
        }
    }
    return image;
}
